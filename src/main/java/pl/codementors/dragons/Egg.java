package pl.codementors.dragons;

import java.util.Objects;

public class Egg {
    private Colour colour;

    public Egg(Dragon dragon) {
        colour = dragon.getColour();
    }

    public Colour getColour() {
        return colour;
    }

    public void setColour(Colour colour) {
        this.colour = colour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Egg egg = (Egg) o;
        return colour == egg.colour;
    }

    @Override
    public int hashCode() {
        return Objects.hash(colour);
    }

    @Override
    public String toString() {
        return "Egg{" +
                "colour=" + colour +
                '}';
    }
}
