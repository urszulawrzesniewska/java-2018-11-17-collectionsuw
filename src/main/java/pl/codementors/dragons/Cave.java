package pl.codementors.dragons;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class Cave {

    private static final Logger log = Logger.getLogger(Cave.class.getName());
    private List<Dragon> dragons = new ArrayList<>();

    public void add(Dragon dragon) {

        dragons.add(dragon);
    }

    public void remove(Dragon dragon) {
        dragons.remove(dragon);
    }

    public void read(String fileName) {

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while (br.ready()) {
                String colour1 = br.readLine();
                String wingspanText = br.readLine();
                String ageText = br.readLine();
                String name = br.readLine();

                Colour colour = Colour.valueOf(colour1.toUpperCase());
                int age = Integer.parseInt(ageText);
                int wingspan = Integer.parseInt(wingspanText);

                Dragon dragon = new Dragon(colour, wingspan, age, name);
                dragons.add(dragon);
            }
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    public void print() {

        dragons.forEach(System.out::println);
        System.out.println();
    }

    public void printNames() {
        dragons.stream().map(Dragon::getName).forEach(System.out::println);
    }

    public void printDescription() {
        dragons.forEach(e -> System.out.println(e.getName() + " " + e.getColour()));
    }

    public List<Dragon> sorted() {
        return dragons.stream().sorted().collect(Collectors.toList());
    }

    public List<Dragon> sortbyName() {
        return dragons.stream()
                .sorted(Comparator.comparing(Dragon::getName)).collect(Collectors.toList());
    }

    public List<Dragon> sortByAge() {
        return dragons.stream()
                .sorted(Comparator.comparingInt(Dragon::getAge)).collect(Collectors.toList());
    }

    public int maxAge() {
        return dragons.stream().mapToInt(Dragon::getAge).max().orElse(0);
    }

    public int maxWingspan(int i) {
        return dragons.stream().mapToInt(Dragon::getWingspan).max().orElse(0);
    }

    public List<Integer> namesLength() {
        return dragons.stream().map(e -> e.getName().length()).collect(Collectors.toList());
    }

    public String maxName() {
        return dragons.stream().map(Dragon::getName).max(Comparator.comparingInt(String::length)).orElse(null);
    }

    public List<Dragon> fetch(Colour silver, String... colour) {
        List<String> colourList = Arrays.asList(colour);
        return dragons.stream()
                .filter(e -> e.getColour().equals(Colour.SILVER))
                .filter(e -> colourList.contains(e.getColour()))
                .collect(Collectors.toList());
    }

    public Set<Enum> fetchColour() {
        return dragons.stream().map(Dragon::getColour).collect(Collectors.toSet());
    }

    public List<String> namelist() {
        return dragons.stream().map(Dragon::getName).collect(Collectors.toList());
    }

    List colour = Arrays.asList(Colour.values());

    {
        if (colour.contains(Colour.valueOf("SILVER"))) {
            System.out.println("TRUE");
        }
    }
    public static boolean isAgeoutOfBounds(int i, int[] age){
        for(i = 0; i < age.length; i++){
            if (age[i] < 100) {
                return false;
            }
        }
        return true;
    }
    public List<Egg> eggs() {
        return dragons.stream().map(Egg::new).collect(Collectors.toList());
    }
}

