package pl.codementors.dragons;

import java.util.Objects;

public class Dragon {
    private Colour colour;
    private int age;
    private int wingspan;
    private String name;

    public Colour getColour() {
        return colour;
    }

    public void setColour(Colour colour) {
        this.colour = colour;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dragon dragon = (Dragon) o;
        return age == dragon.age &&
                wingspan == dragon.wingspan &&
                colour == dragon.colour &&
                Objects.equals(name, dragon.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(colour, age, wingspan, name);
    }

    @Override
    public String toString() {
        return "Dragon{" +
                "colour=" + colour +
                ", age=" + age +
                ", wingspan=" + wingspan +
                ", name='" + name + '\'' +
                '}';
    }

    public Dragon(Colour colour, int wingspan, int age, String name) {
        super();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
