package pl.codementors.dragons;

public enum Colour {

    BLACK, RED, GREEN, SILVER, GOLDEN
}
